package FS_combination;

import FS_BasicEmailSubScription.FS_EmailSubscription;
import FS_EdConsultant.FS_fatherGuardian;
import FS_EdConsultant.FS_motherGuardian;
import FS_EdConsultant.FS_overThirteen;
import FS_EdConsultant.FS_staffMember;
import FS_applicationList.FS_getapplicationList;
import FS_connect.*;
import FS_connect.FS_underThirteen;
import FS_requestToolkit.FS_requestToolkitCanada;
import FS_requestToolkit.FS_requestToolkitUsa;
import FS_sanpchat.FS_snapchatLead;
import FS_sanpchat.FS_snapchatSignUp;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class FS_NABIChromeTest {

    WebDriver driver;
    String strDate;

    @Test(priority = 0)
    public void launchBrowser() {
        System.out.println("launching chrome browser");
        ChromeOptions options = new ChromeOptions();
//                  System.setProperty("webdriver.chrome.driver", System.getProperty("user.home") + "/bin/chromedriver");
        options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors", "--silent");
        driver = new ChromeDriver(options);
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("MMMddyyHHmmss");
        strDate = dateFormat.format(date);
    }

    @Test(priority = 1)
    public void checkEmailSubscription() throws InterruptedException {
        FS_EmailSubscription es = new FS_EmailSubscription();
        launchBrowser();
        es.checkEmailSubscription(driver, strDate);
    }

    @Test(priority = 2)
    public void checkapplicationListPage() throws InterruptedException {
        FS_getapplicationList ga = new FS_getapplicationList();
        ga.openApplication(driver);
        ga.filloutEmail(driver, strDate);
        ga.submitForm(driver);
    }

    @Test(priority = 3)
    public void checkConnectFormPage() throws InterruptedException {

        FS_Connect_underThirteen scenario1 = new FS_Connect_underThirteen();
        scenario1.openApplication(driver);
        scenario1.enterDataFirstName(driver);
        scenario1.enterDataLastName(driver);
        scenario1.selectIamAUndr13(driver);
        scenario1.enteremail(driver, strDate);
        scenario1.enterZipCode(driver);
        scenario1.enterMessge(driver);
        scenario1.submitForm(driver);

        FS_Connect_overThirteen scenario2 = new FS_Connect_overThirteen();
        scenario2.openApplication(driver);
        scenario2.enterDataFirstName(driver);
        scenario2.enterDataLastName(driver);
        scenario2.selectIamAUndr13(driver);
        scenario2.enteremail(driver, strDate);
        scenario2.enterZipCode(driver);
        scenario2.enterMessge(driver);
        scenario2.submitForm(driver);

        FS_Connect_MotherGuardian scenario3 = new FS_Connect_MotherGuardian();
        scenario3.openApplication(driver);
        scenario3.enterDataFirstName(driver);
        scenario3.enterDataLastName(driver);
        scenario3.selectIamAUndr13(driver);
        scenario3.enteremail(driver, strDate);
        scenario3.enterZipCode(driver);
        scenario3.enterMessge(driver);
        scenario3.submitForm(driver);

        FS_Connect_FatherGuardian scenario4 = new FS_Connect_FatherGuardian();
        scenario4.openApplication(driver);
        scenario4.enterDataFirstName(driver);
        scenario4.enterDataLastName(driver);
        scenario4.selectIamAUndr13(driver);
        scenario4.enteremail(driver, strDate);
        scenario4.enterZipCode(driver);
        scenario4.enterMessge(driver);
        scenario4.submitForm(driver);

        FS_Connect_staffMember scenario5 = new FS_Connect_staffMember();
        scenario5.openApplication(driver);
        scenario5.enterDataFirstName(driver);
        scenario5.enterDataLastName(driver);
        scenario5.selectIamAUndr13(driver);
        scenario5.enteremail(driver, strDate);
        scenario5.enterZipCode(driver);
        scenario5.enterMessge(driver);
        scenario5.submitForm(driver);

    }

    @Test(priority = 4)
    public void checkEdConsultantPage() throws InterruptedException {

        FS_underThirteen scenario1 = new FS_underThirteen();
        scenario1.openApplication(driver);
        scenario1.enterDataFirstName(driver);
        scenario1.enterDataLastName(driver);
        scenario1.enteremail(driver, strDate);
        scenario1.enterZipCode(driver);
        scenario1.selectIamAUndr13(driver);
        scenario1.submitForm(driver);

        FS_overThirteen scenario2 = new FS_overThirteen();
        scenario2.openApplication(driver);
        scenario2.enterDataFirstName(driver);
        scenario2.enterDataLastName(driver);
        scenario2.enteremail(driver, strDate);
        scenario2.enterZipCode(driver);
        scenario2.selectIamAUndr13(driver);
        scenario2.submitForm(driver);

        FS_fatherGuardian scenario3 = new FS_fatherGuardian();
        scenario3.openApplication(driver);
        scenario3.enterDataFirstName(driver);
        scenario3.enterDataLastName(driver);
        scenario3.enteremail(driver, strDate);
        scenario3.enterZipCode(driver);
        scenario3.selectIamAUndr13(driver);
        scenario3.submitForm(driver);

        FS_motherGuardian scenario4 = new FS_motherGuardian();
        scenario4.openApplication(driver);
        scenario4.enterDataFirstName(driver);
        scenario4.enterDataLastName(driver);
        scenario4.enteremail(driver, strDate);
        scenario4.enterZipCode(driver);
        scenario4.selectIamAUndr13(driver);
        scenario4.submitForm(driver);



    }

    @Test(priority = 5)
    public void checkRequestCatalogPageUsa() throws InterruptedException {

        FS_requestCatalogUsa.FS_reqCatStuO13 scenario1 = new FS_requestCatalogUsa.FS_reqCatStuO13();
        scenario1.openApplication(driver);
        scenario1.enterSchool(driver);
        scenario1.selectIamAUndr13(driver);
        scenario1.enterDataFirstName(driver);
        scenario1.enterDataLastName(driver);
        scenario1.enteremail(driver, strDate);
        scenario1.enterAddress(driver);
        scenario1.enterCity(driver);
        scenario1.enterState(driver);
        scenario1.enterZipCode(driver);
        scenario1.enterCountry(driver);
        scenario1.enterPhoneNumber(driver);
        scenario1.enterStudentInfo(driver);
        scenario1.submitForm(driver);

        FS_requestCatalogUsa.FS_reqCatStuO13 scenario2 = new FS_requestCatalogUsa.FS_reqCatStuO13();
        scenario2.openApplication(driver);
        scenario2.enterSchool(driver);
        scenario2.selectIamAUndr13(driver);
        scenario2.enterDataFirstName(driver);
        scenario2.enterDataLastName(driver);
        scenario2.enteremail(driver, strDate);
        scenario2.enterAddress(driver);
        scenario2.enterCity(driver);
        scenario2.enterState(driver);
        scenario2.enterZipCode(driver);
        scenario2.enterCountry(driver);
        scenario2.enterPhoneNumber(driver);
        scenario2.enterStudentInfo(driver);
        scenario2.submitForm(driver);

        FS_requestCatalogUsa.FS_reqCatMotherGurdian scenario3 = new FS_requestCatalogUsa.FS_reqCatMotherGurdian();
        scenario3.openApplication(driver);
        scenario3.enterSchool(driver);
        scenario3.selectIamAUndr13(driver);
        scenario3.enterDataFirstName(driver);
        scenario3.enterDataLastName(driver);
        scenario3.enteremail(driver, strDate);
        scenario3.enterAddress(driver);
        scenario3.enterCity(driver);
        scenario3.enterState(driver);
        scenario3.enterZipCode(driver);
        scenario3.enterCountry(driver);
        scenario3.enterPhoneNumber(driver);
        scenario3.enterStudentInfo(driver);
        scenario3.submitForm(driver);

        FS_requestCatalogUsa.FS_reqCatFatherGuardian scenario4 = new FS_requestCatalogUsa.FS_reqCatFatherGuardian();
        scenario4.openApplication(driver);
        scenario4.enterSchool(driver);
        scenario4.selectIamAUndr13(driver);
        scenario4.enterDataFirstName(driver);
        scenario4.enterDataLastName(driver);
        scenario4.enteremail(driver, strDate);
        scenario4.enterAddress(driver);
        scenario4.enterCity(driver);
        scenario4.enterState(driver);
        scenario4.enterZipCode(driver);
        scenario4.enterCountry(driver);
        scenario4.enterPhoneNumber(driver);
        scenario4.enterStudentInfo(driver);
        scenario4.submitForm(driver);


        FS_requestCatalogUsa.FS_reqCatStaffMember scenario5 = new FS_requestCatalogUsa.FS_reqCatStaffMember();
        scenario5.openApplication(driver);
        scenario5.enterSchool(driver);
        scenario5.selectIamAUndr13(driver);
        scenario5.enterDataFirstName(driver);
        scenario5.enterDataLastName(driver);
        scenario5.enteremail(driver, strDate);
        scenario5.enterAddress(driver);
        scenario5.enterCity(driver);
        scenario5.enterState(driver);
        scenario5.enterZipCode(driver);
        scenario5.enterCountry(driver);
        scenario5.enterPhoneNumber(driver);
        scenario5.enterStudentInfo(driver);
        scenario5.submitForm(driver);

    }

    @Test(priority = 6)
    public void checkRequestCatalogPageCanada() throws InterruptedException {

        FS_requestCatalogCanada.FS_reqCatStuU13 scenario1 = new FS_requestCatalogCanada.FS_reqCatStuU13();
        scenario1.openApplication(driver);
        scenario1.enterSchool(driver);
        scenario1.selectIamAUndr13(driver);
        scenario1.enterDataFirstName(driver);
        scenario1.enterDataLastName(driver);
        scenario1.enteremail(driver, strDate);
        scenario1.enterAddress(driver);
        scenario1.enterCity(driver);
        scenario1.enterState(driver);
        scenario1.enterZipCode(driver);
        scenario1.enterCountry(driver);
        scenario1.enterPhoneNumber(driver);
        scenario1.enterStudentInfo(driver);
        scenario1.submitForm(driver);

        FS_requestCatalogCanada.FS_reqCatStuO13 scenario2 = new FS_requestCatalogCanada.FS_reqCatStuO13();
        scenario2.openApplication(driver);
        scenario2.enterSchool(driver);
        scenario2.selectIamAUndr13(driver);
        scenario2.enterDataFirstName(driver);
        scenario2.enterDataLastName(driver);
        scenario2.enteremail(driver, strDate);
        scenario2.enterAddress(driver);
        scenario2.enterCity(driver);
        scenario2.enterState(driver);
        scenario2.enterZipCode(driver);
        scenario2.enterCountry(driver);
        scenario2.enterPhoneNumber(driver);
        scenario2.enterStudentInfo(driver);
        scenario2.submitForm(driver);

        FS_requestCatalogCanada.FS_reqCatMotherGurdian scenario3 = new FS_requestCatalogCanada.FS_reqCatMotherGurdian();
        scenario3.openApplication(driver);
        scenario3.enterSchool(driver);
        scenario3.selectIamAUndr13(driver);
        scenario3.enterDataFirstName(driver);
        scenario3.enterDataLastName(driver);
        scenario3.enteremail(driver, strDate);
        scenario3.enterAddress(driver);
        scenario3.enterCity(driver);
        scenario3.enterState(driver);
        scenario3.enterZipCode(driver);
        scenario3.enterCountry(driver);
        scenario3.enterPhoneNumber(driver);
        scenario3.enterStudentInfo(driver);
        scenario3.submitForm(driver);

        FS_requestCatalogCanada.FS_reqCatFatherGuardian scenario4 = new FS_requestCatalogCanada.FS_reqCatFatherGuardian();
        scenario4.openApplication(driver);
        scenario4.enterSchool(driver);
        scenario4.selectIamAUndr13(driver);
        scenario4.enterDataFirstName(driver);
        scenario4.enterDataLastName(driver);
        scenario4.enteremail(driver, strDate);
        scenario4.enterAddress(driver);
        scenario4.enterCity(driver);
        scenario4.enterState(driver);
        scenario4.enterZipCode(driver);
        scenario4.enterCountry(driver);
        scenario4.enterPhoneNumber(driver);
        scenario4.enterStudentInfo(driver);
        scenario4.submitForm(driver);


        FS_requestCatalogCanada.FS_reqCatStaffMember scenario5 = new FS_requestCatalogCanada.FS_reqCatStaffMember();
        scenario5.openApplication(driver);
        scenario5.enterSchool(driver);
        scenario5.selectIamAUndr13(driver);
        scenario5.enterDataFirstName(driver);
        scenario5.enterDataLastName(driver);
        scenario5.enteremail(driver, strDate);
        scenario5.enterAddress(driver);
        scenario5.enterCity(driver);
        scenario5.enterState(driver);
        scenario5.enterZipCode(driver);
        scenario5.enterCountry(driver);
        scenario5.enterPhoneNumber(driver);
        scenario5.enterStudentInfo(driver);
        scenario5.submitForm(driver);

    }

    @Test(priority = 7)
    public void checkRequestToolkitPage() throws InterruptedException {

        FS_requestToolkitUsa scenario1 = new FS_requestToolkitUsa();
        scenario1.openApplication(driver);
        scenario1.enterDataFirstName(driver);
        scenario1.enterDataLastName(driver);
        scenario1.enterAddress(driver);
        scenario1.enterCity(driver);
        scenario1.enterState(driver);
        scenario1.enteremail(driver, strDate);
        scenario1.enterZipCode(driver);
        scenario1.enterCountry(driver);
        scenario1.submitForm(driver);

        FS_requestToolkitCanada scenario2 = new FS_requestToolkitCanada();
        scenario2.openApplication(driver);
        scenario2.enterDataFirstName(driver);
        scenario2.enterDataLastName(driver);
        scenario2.enterAddress(driver);
        scenario2.enterCity(driver);
        scenario2.enterState(driver);
        scenario2.enteremail(driver, strDate);
        scenario2.enterZipCode(driver);
        scenario2.enterCountry(driver);
        scenario2.submitForm(driver);

    }

    @Test(priority = 8)
    public void checkSnapchatPage() throws InterruptedException {

        FS_snapchatLead scenario1 = new FS_snapchatLead();
        scenario1.openApplication(driver);
        scenario1.filloutForm(driver, strDate);
        scenario1.submitForm(driver);

        FS_snapchatSignUp scenario2 = new FS_snapchatSignUp();
        scenario2.openApplication(driver);
        scenario2.filloutForm(driver, strDate);
        scenario2.submitForm(driver);
    }

}
