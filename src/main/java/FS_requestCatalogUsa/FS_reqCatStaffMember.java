package FS_requestCatalogUsa;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;

//FOUR school and STUDENT STAFF-MEMEBER combination
public class FS_reqCatStaffMember {

    WebDriver driver;

    //@Test(priority = 0)
    public void launchBrowser(WebDriver driver) {
        Reporter.log("This test will verify the firefox browser launch");
        System.out.println("launching firefox browser");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.home") + "/bin/geckodriver");
        driver = new FirefoxDriver();
    }

    //@Test(priority = 1)
    public void openApplication(WebDriver driver) throws InterruptedException {
        Reporter.log("This test will open the fusionSpan website on firefox");
        driver.get("https://readyformore.com/find-a-school/request-a-catalog");
    }

    //@Test(priority = 2)
    public void enterSchool(WebDriver driver) throws InterruptedException {
        Reporter.log("This test will add three school over 13 student");
        driver.findElement(By.xpath("//label[contains(text(),'Catalogs')]/../div")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//label[contains(text(),'Catalogs')]/../div/div/div[9]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//label[contains(text(),'Catalogs')]/../div/div/div[21]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//label[contains(text(),'Catalogs')]/../div/div/div[31]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//label[contains(text(),'Catalogs')]/../div/div/div[33]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"request-form-ext\"]/form/div[1]/div[1]/fieldset/div/i")).click();
    }

    //@Test(priority = 3)
    public void selectIamAUndr13(WebDriver driver) throws InterruptedException {
        Reporter.log("Selecting from I Am a");
        WebElement dd = driver
                .findElement(By.xpath("//*[@id=\"request-form-ext\"]/form/div[1]/div[2]/fieldset/div"));
        dd.click();
        Thread.sleep(2000);
        WebElement under13 = driver
                .findElement(By.xpath("//*[@id=\"request-form-ext\"]/form/div[1]/div[2]/fieldset/div/div/div[5]"));
        under13.click();
    }

    //@Test(priority = 4)
    public void enteremail(WebDriver driver,String date) throws InterruptedException {
        Reporter.log("Entering Email address");
        WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                email);
        email.click();
        email.clear();
        String emailID = "spudari+" + date + "catStaffMem@fusionspan.com";
        email.sendKeys(emailID);
    }

    //@Test(priority = 5)
    public void enterDataFirstName(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering data in first name field");
        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"first_name\"]"));
        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js1.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                firstName);
        firstName.click();
        firstName.clear();
        firstName.sendKeys("pallavi-SM-test");
    }

    //@Test(priority = 6)
    public void enterDataLastName(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering data in Last name field");
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"last_name\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                lastName);
        lastName.click();
        lastName.clear();
        lastName.sendKeys("keshri-SM-test");
    }

    //@Test(priority = 7)
    public void enterAddress(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  address");
        WebElement address = driver.findElement(By.xpath("//*[@id=\"address_one\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                address);
        address.click();
        address.clear();
        address.sendKeys("12300 Twinbrook Pkwy");
    }

    //@Test(priority = 8)
    public void enterCity(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  city");
        WebElement city = driver.findElement(By.xpath("//*[@id=\"city\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                city);
        city.click();
        city.clear();
        city.sendKeys("Rockville");
    }

    //@Test(priority = 9)
    public void enterState(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  state");
        WebElement state = driver.findElement(By.xpath("//label[contains(text(),'State')]/../div/input"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                state);
        state.click();
        state.findElement(By.xpath("//label[contains(text(),'State')]/../div/input/../div[2]//span[text()=\"Maryland\"]/..")).click();
    }

    //@Test(priority = 10)
    public void enterZipCode(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering Zip Code");
        WebElement ZipCode = driver.findElement(By.xpath("//*[@id=\"zip_code\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                ZipCode);
        ZipCode.click();
        ZipCode.clear();
        ZipCode.sendKeys("20852");
    }

    //@Test(priority = 11)
    public void enterCountry(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  country");
        WebElement country = driver.findElement(By.xpath("//label[contains(text(),'Country')]/../div[1]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                country);
        country.click();
        country.findElement(By.xpath("//label[contains(text(),'Country')]/../div[1]/div[2]//span[text()=\"United States\"]/..")).click();
    }

    //@Test(priority = 12)
    public void enterPhoneNumber(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  phoneNumber");
        WebElement address = driver.findElement(By.xpath("//*[@id=\"phone_number\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                address);
        address.click();
        address.clear();
        address.sendKeys("9999999999");
    }

    //@Test(priority = 13)
    public void enterStudentInfo(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  studentInfo");

        driver.findElement(By.id("student_first_name")).sendKeys("pallavi-SM-test");
        driver.findElement(By.id("student_last_name")).sendKeys("keshri-SM-test");

        WebElement gender = driver.findElement(By.xpath("//label[contains(text(),'Gender')]/../div"));
        gender.click();
        gender.findElement(By.xpath("//label[contains(text(),'Gender')]/../div/div[2]/div[3]")).click();

        WebElement citizenship = driver.findElement(By.xpath("//label[contains(text(),'Citizenship')]/../div"));
        citizenship.click();
        citizenship.findElement(By.xpath("//label[contains(text(),'Citizenship')]/../div/div[2]/div[2]")).click();


        WebElement grade = driver.findElement(By.xpath("//label[contains(text(),'Grade')]/../div"));
        grade.click();
        grade.findElement(By.xpath("//label[contains(text(),'Grade')]/../div/div[2]/div[5]")).click();

    }

    //@Test(priority = 14)
    public void submitForm(WebDriver driver) throws InterruptedException {
        String expectedSuccessUrl = "https://readyformore.com/thank-you";
        Reporter.log("Submitting the form by clicking on Submit button");
        WebElement submit = driver.findElement(By.xpath("//button[contains(text(),'Submit')]"));
        submit.click();
        Thread.sleep(6000);
        String actualsuccessUrl = driver.getCurrentUrl();
        System.out.println("actual:" + actualsuccessUrl);
        Assert.assertEquals(actualsuccessUrl, expectedSuccessUrl, "::::::::::::Send My Message submission was UNSUCCESSFULL:::::;");

    }

    //@Test(priority = 15)
    public void closebrowser(WebDriver driver) {
        driver.quit();

    }
}
