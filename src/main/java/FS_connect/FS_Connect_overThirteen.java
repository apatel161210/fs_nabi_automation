package FS_connect;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;

public class FS_Connect_overThirteen {

    WebDriver driver;

    //@Test(priority = 0)
    public void launchBrowser(WebDriver driver) {

        Reporter.log("This test will verify the firefox browser launch");

        System.out.println("launching firefox browser");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.home") + "/bin/geckodriver");
        driver = new FirefoxDriver();
    }


    //@Test(priority = 1)
    public void openApplication(WebDriver driver) throws InterruptedException {

        Reporter.log("This test will open the fusionSpan website on firefox");
        driver.get("https://readyformore.com/connect");

    }

    //@Test(priority = 2)
    public void enterDataFirstName(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering data in first name field");
        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"first_name\"]"));
        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js1.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                firstName);
        firstName.click();
        firstName.clear();
        firstName.sendKeys("firstname-overthirteen");

    }

    //@Test(priority = 3)
    public void enterDataLastName(WebDriver driver) {
        Reporter.log("Entering data in Last name field");
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"last_name\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                lastName);
        lastName.click();
        lastName.clear();
        lastName.sendKeys("lastname-overthirteen");
    }

    //@Test(priority = 4)
    public void enteremail(WebDriver driver, String date) {
        Reporter.log("Entering Email address");
        WebElement email = driver.findElement(By.xpath("//*[@id=\"email_address\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                email);
        email.click();
        email.clear();
        String emailID = "spudari+" + date + "ConStuO" +
                "13@fusionspan.com";
        email.sendKeys(emailID);

    }

    //@Test(priority = 5)
    public void enterZipCode(WebDriver driver) {
        Reporter.log("Entering Zip Code");
        WebElement ZipCode = driver.findElement(By.xpath("//*[@id=\"zip_code\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                ZipCode);
        ZipCode.click();
        ZipCode.clear();
        ZipCode.sendKeys("20853");
    }

    //@Test(priority = 6)
    public void selectIamAUndr13(WebDriver driver) throws InterruptedException {
        Reporter.log("Selecting from I Am a");

        String ddValue = "overAgeOf13";
        WebElement under13s = driver
                .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-1\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                under13s);

        if (ddValue == "underAgeOf13") {

            WebElement dd = driver
                    .findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[4]/div/div[1]/div"));
            dd.click();
            Thread.sleep(2000);
            WebElement under13 = driver
                    .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-1\"]"));
            under13.click();

        } else if (ddValue == "overAgeOf13") {

            WebElement dd = driver
                    .findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[4]/div/div[1]/div"));
            dd.click();
            Thread.sleep(2000);
            WebElement under13 = driver
                    .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-2\"]"));
            under13.click();
        } else if (ddValue == "mothrGuarOfStudent") {
            WebElement dd = driver
                    .findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[4]/div/div[1]/div"));
            dd.click();
            Thread.sleep(2000);
            WebElement under13 = driver
                    .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-3\"]"));
            under13.click();

        } else if (ddValue == "fathrGuarOfStudent") {
            WebElement dd = driver
                    .findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[4]/div/div[1]/div"));
            dd.click();
            Thread.sleep(2000);
            WebElement under13 = driver
                    .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-4\"]"));
            under13.click();
        } else if (ddValue == "schoolStffMem") {
            WebElement dd = driver
                    .findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[4]/div/div[1]/div"));
            dd.click();
            Thread.sleep(2000);
            WebElement under13 = driver
                    .findElement(By.xpath("//*[@id=\"choices--choices-single-default-item-choice-5\"]"));
            under13.click();
        }
    }

    //@Test(priority = 7)
    public void enterMessge(WebDriver driver) {
        Reporter.log("Entering Message");
        WebElement ZipCode = driver.findElement(By.xpath("//*[@id=\"message\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                ZipCode);
        ZipCode.click();
        ZipCode.clear();
        ZipCode.sendKeys("This is test for over 13");
    }

    //@Test(priority = 8)
    public void submitForm(WebDriver driver) throws InterruptedException {

        String expectedSuccessUrl = "https://readyformore.com/connect?form=connect&success=true";
        Reporter.log("Submitting the form by clicking on Submit button");
        WebElement sendMyMessage = driver.findElement(By.xpath("/html/body/main/div/article/div/section[3]/form/div[7]/button"));
        sendMyMessage.click();
        Thread.sleep(7000);
        String actualsuccessUrl = driver.getCurrentUrl();
        System.out.println("actual:" + actualsuccessUrl);
        Assert.assertEquals(actualsuccessUrl, expectedSuccessUrl, "::::::::::::Send My Message submission was UNSUCCESSFULL:::::;");
    }

    //@Test(priority = 18)
    public void closebrowser(WebDriver driver) {
        driver.quit();

    }

}
