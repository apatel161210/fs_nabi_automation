package FS_applicationList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;

public class FS_getapplicationList {


    //@Test(priority = 0)
    public void launchBrowser(WebDriver driver) {
        Reporter.log("This test will verify the firefox browser launch");
        System.out.println("launching firefox browser");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.home") + "/bin/geckodriver");
        driver = new FirefoxDriver();
    }

    //@Test(priority = 1)
    public void openApplication(WebDriver driver) throws InterruptedException {
        Reporter.log("This test will open the fusionSpan website on firefox");
        driver.get("https://readyformore.com/how-to-apply/applications");
    }

    //@Test(priority = 2)
    public void filloutEmail(WebDriver driver, String date) throws InterruptedException {
        Reporter.log("This test will fill out email");
        String emailID = "spudari+" + date + "appList@fusionspan.com";
        driver.findElement(By.name("email")).sendKeys(emailID);
    }

    //@Test(priority = 3)
    public void submitForm(WebDriver driver) throws InterruptedException {
        Reporter.log("This test will submit the form");
        String expectedUrl = "https://readyformore.com/how-to-apply/applications?form=email&success=true#gated-download-form-success";
        Thread.sleep(1000);
        driver.findElement(By.name("submit")).click();
        Thread.sleep(8000);
        String actualUrl = driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, expectedUrl, "::::::::::::Send My Message submission was UNSUCCESSFULL:::::;");
    }

    //@Test(priority = 4)
    public void closebrowser(WebDriver driver) {
        driver.quit();
    }

}